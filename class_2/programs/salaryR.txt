########################################
# [1] data 
########################################

# (sal.0 = read.table('C:\\ ....\\salary.txt', h = T));  

########################################
# remove outlier at row 33
########################################  

sal = sal.0[-33, ];  

rownames(sal) = c(1:45);

cat('', sep = '\n'); str(sal);   

cat('', sep = '\n'); (n = nrow(sal)); 

#########################################
# [2] change column names;
#     add class = E + 3*M: 1, 2, ..., 6; 
#     use transform(sal,*)
######################################### 

sal = transform(sal, 
S = S/1E3, E = as.factor(E), 
M = as.factor(M), class = as.factor(E + 3*M));

names(sal)[1:4] = c('salx1000',
'years', 
'educ',
'man1'); 

cat('', sep = '\n'); str(sal); 

cat('', sep = '\n'); head(sal, n = 10);   

########################################
# [3] initial scatter plot (!?)
######################################## 

xrange = with(sal, range(years));
 
yrange = with(sal,range(salx1000));

attach(sal)

windows();

plot(c(min(years)-1,max(years)), 
c(min(salx1000), max(salx1000)+3), 
type = 'n', 
xlab = 'years', 
ylab = 'salx1000',
main = 'correspondence of points to classes (!?)'); 

points(years,salx1000); 

########################################
# [4] scatter plot by class
########################################

nclass = max(as.integer(class)); 

colors = rainbow(nclass);

linetype = c(1:nclass);

plotchar = c(15:20);  

########################################
# [5] set up display
########################################

windows();

par(lwd = 2., 
font.lab = 2, cex.lab = 1.1,
font.main = 2, cex.main = 1.4, 
col.main = 'deepskyblue');

plot(c(min(years)-1,max(years)), 
c(min(salx1000), max(salx1000)+3), 
type = 'n', 
xlab = 'years', 
ylab = 'salx1000', 
main = 'Salary example');  

########################################
# [6] add lines with color by class
########################################

for (i in 1:nclass)

{
  par(cex = 1.5, 
  col = colors[i], 
  lty = linetype[i],
  pch = plotchar[i], 
  lwd = 3.5);

  sal. = subset(sal, class == as.character(i)); 

  with(sal.,
  lines(years, salx1000, type = 'b'));

  rm(sal.); 
}

########################################
# [7] add a legend
######################################## 

par(col = 'black',  
bg = 'ghostwhite', 
lty = 1,
lwd = 1); 

legend(min(years)-1., max(salx1000)+3.,
h = T,
xjust = 0,
cex = 0.5,
pt.cex = 0.9,
1:nclass,  
col = colors, 
pch = plotchar, 
lty = linetype, 
lwd = rep(2.,6),
title = 'class');  

detach(sal) 

########################################
# [8] ls in full 12-parameter model
######################################## 

f = salx1000 ~ class + class:years - 1; 

model.exp = lm(f,sal); 

coef.exp = with(summary(model.exp), coefficients);
 
cat('', sep = '\n'); format(round(coef.exp, d = 4)); 

########################################
# [9] ls in reduced 7-parameter model
########################################

f = salx1000 ~ class + years - 1; 

model.red = lm(f, sal);  

coef.red = with(summary(model.red), coefficients);

cat('', sep = '\n'); format(round(coef.red, d = 4));  

#################################
# [10] comparison of models
#################################

cat('', sep = '\n'); (hip = anova(model.red,model.exp)); 

cat('', sep = '\n'); str(hip);  

cat('', sep = '\n'); round(with(hip,RSS), d = 4);  

#################################
# [11] comparison of models
#################################

f = salx1000 ~ years;

model = lm(f,sal); 

coef = with(summary(model), coefficients);

cat('', sep = '\n'); format(round(coef, d = 4));  

cat('', sep = '\n'); (hip = anova(model,model.red)); 

cat('', sep = '\n');  round(with(hip,RSS), d = 4); 

#################################
# [12] residual plots
#################################

library(car);

windows(); 

par(mfrow = c(2,2),
oma = c(2,2,3,2)); 

residualPlots(model.exp, 
type = 'rstudent', 
tests = FALSE, 
quadratic = FALSE,
terms = ~ 1, 
ask = FALSE, 
col = 'navy',
pch = 19,
cex = 1.5); 

title('12 parameter model.exp', font.main = 1, cex.main = 1.2);

residualPlots(model.red, 
type = 'rstudent', 
tests = FALSE, 
quadratic = FALSE,
terms = ~ 1, 
ask = FALSE, 
col = 'firebrick1',
pch = 19,
cex = 1.5); 

title('7 parameter model.red', font.main = 1, cex.main = 1.2);

residualPlots(model, 
type = 'rstudent', 
tests = FALSE, 
quadratic = FALSE,
terms = ~ 1, 
ask = FALSE, 
col = 'mediumseagreen',
pch = 19,
cex = 1.5); 

title('salx1000 ~ years', font.main = 1, cex.main = 1.2);