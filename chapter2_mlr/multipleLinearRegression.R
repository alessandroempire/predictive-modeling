
# Multivariate Linear Regression 
# Developed by ALessandro La Corte

# In Linux the working directory is automatically set...
# setwd(path) 

# Onced installed, not needed to run again:
# install.packages("openxlsx")
# install.packages(lsr)
# install.packages("mvoutlier")

#Loading needed libraries
library("openxlsx")
library(car)
library("mvoutlier")

costData = read.xlsx("cost.xlsx", sheet=1)
costData

###############################################################################
# Make an initial exploratory plot of the data.
###############################################################################

jpeg("costData.jpeg", width=700, height=750, res=90)
plot(costData, pch = 19, 
col = 'royalblue2', cex = 1.3, gap = .15,
font.labels = 2, cex.labels = 1.1, 
main = 'Cost data')
dev.off()


###############################################################################
# Fit a multiple linear regression model_cost including all the regressors.
###############################################################################

#Number of columns
nc = ncol(costData)

#Number of rows (sample size)
nr = nrow(costData)

# Y vector nc x 1
Y = costData[,nc]

#Calcualte the p-value
p = ncol(costData[, 1:nc-1])

# The x matrix: p+1 x nc matrix 
X = cbind(1, costData[, 1:p])

X = data.matrix(X) #Convert X to a numerical object
Y = data.matrix(Y) #Convert Y to a numerical object

# We write the Normal Equation to find beta
# Beta = (X'X)^(-1) * X'Y
# Notes:
# crossprod(A,B) crossprod(A) == A'B and A'A respectively.
# solve(A)	Inverse of A where A is a square matrix.

beta = solve(crossprod(X)) %*% crossprod(X,Y)

colnames(beta) = 'coefficients'

# Note: the previous is equivalent to:
f = cost ~ .

model_cost = lm (f, data = costData)

# coef == beta
coef = with(model_cost, coefficients)
print ("coefficients of the fitted model")
format(round(coef, d=4))


###############################################################################
#Make a diagnostic plot of (studentized) residuals versus fitted values. Is there any need
#of transforming for correcting for some observed anomaly? Which variable is the one
#that seems to be most responsible for that? Explain
###############################################################################

#Load car library for function residuals plot

png("residualsPlotCost.png", width=700, height=750, res=90)
residualPlots(model_cost, 
				type = 'rstudent',
				tests = FALSE, 
				quadratic = FALSE,
				ask = FALSE, 
				col = 'darkcyan',
				pch = 19,
				cex = 1.5, 
				main = 'Residual plots cost')
dev.off()

png("AVPlotCost.png", width=700, height=750, res=90)
avPlots(model_cost, pch = 16, col = 'royalblue2', grid = F, 
main = 'Added-Variable Plots ')
dev.off()

png("CRPlotCost.png", width=700, height=750, res=90)
crPlots(model_cost, pch = 16, col = 'royalblue2', grid = F, 
main = 'Component + Residual Plots ')
dev.off()

# The four variable show outliers that might be causing the anomalies
# Data transformation is needed 

#So let's calculate a new model over the data taking the logs over the 
# machinery, raw and energy attributes and salary

#See the ranges of each variable
with(costData,range(machinery))
with(costData,range(raw))
with(costData,range(energy))
with(costData,range(salary))

#Procedded to eliminate outliers
# Use library mvoutlier to eliminate visually the outliers
# The function chisq.plot plots the ordered robust mahalanobis distances of the data against the quantiles
# of the Chi-squared distribution. By user interaction this plotting is iterated each time leaving
# out the observation with the greatest distance

indices = chisq.plot(costData)

#The indices choseen visuallt to be removed are: 
 #127 126 125 119 96 113 100 122 81 115 111 121 114 124 91 93 97 117 112 58

# cost_transformed_data_WOutliers = costData
cost_transformed_data_WOutliers = costData[-indices$outliers,]

#Plot new data
png("costDataWithLogs.png", width=700, height=750, res=90)
plot( log(cost_transformed_data_WOutliers), col = 'royalblue2', pch = 16, 
		cex = 1.1, cex.labels = 1.3, 
		font.labels = 2, gap = 0.15,
		labels = c('log(cost) ', 'log(machinery)', 'log(raw)', 'log(energy)', 'log(salary)'), 
		main = 'costData with logs and without outliers')
dev.off()


# Proceded to fitting the model
f_logs = log(cost) ~ log(machinery) + log(raw) + log(energy) + log(salary)

model_cost_logs = lm(f_logs, data=cost_transformed_data_WOutliers)

print ("coefficients of the fitted model with logs")
model_cost_logs

print("Summary of the fitted model with logs")
summary(model_cost_logs)

print("Information of each coefficient of the fitted model with logs")
format(round(coef(summary(model_cost_logs)), d = 4))

#Check the model with logs
png("residualsPlotCostWithLogs.png", width=700, height=750, res=90)
residualPlots(model_cost_logs, 
				type = 'rstudent',
				tests = FALSE, 
				quadratic = FALSE,
				ask = FALSE, 
				col = 'darkcyan',
				pch = 19,
				cex = 1.5, 
				main = 'Residual plots of Cost Data  with logs and without outliers')
dev.off()


png("AVPlotCostWithLogs.png", width=700, height=750, res=90)
avPlots(model_cost_logs, pch = 16, col = 'royalblue2', grid = F, 
	main = 'Added-Variable Plots (after transforming Cost Data)');
dev.off()

png("CRPlotCostWithLogs.png", width=700, height=750, res=90)
crPlots(model_cost_logs, pch = 16, col = 'royalblue2', grid = F, 
	main = 'Component + Residual Plots (after transforming Cost Data)') 
dev.off()



###############################################################################
#After the decision in part (c):
###############################################################################

	###############################################################################
	#i. Analyze the individual significance of all the regressors.
	###############################################################################

	#Setting a confidence interval at 99%, alpha = 0.01
	cost_significance_level = 0.01

	print("Is the variable machinery significant to the model")
	summary(model_cost_logs)$coefficients[2,4] < cost_significance_level
	summary(model_cost_logs)$coefficients[2,4]

	print("Is the variable raw significant to the model")
	summary(model_cost_logs)$coefficients[3,4] < cost_significance_level
	summary(model_cost_logs)$coefficients[3,4]
		
	print("Is the variable energy significant to the model")
	summary(model_cost_logs)$coefficients[4,4] < cost_significance_level 
	summary(model_cost_logs)$coefficients[4,4]

	print("Is the variable salary significant to the model")
	summary(model_cost_logs)$coefficients[5,4] < cost_significance_level
	summary(model_cost_logs)$coefficients[5,4]


	###############################################################################
	##ii. Obtain 95% confidence intervals for all the slope parameters.
	###############################################################################

	costData_log_ci = round(confint(model_cost_logs, level = .95), d = 4);
	print("Confidence Interval for all slope parameters")
	costData_log_ci

	###############################################################################
	#iii. What would be the the predicted costData for the values 
	# x1 (machinery) = 80 
	# x2 (raw) = 3000 
	# x3 (energy) = 1000 
	# x4 (salary) = 25.
	###############################################################################

	new_data = data.frame(machinery=80, raw=3000, energy=1000, salary=25)

	print("Predicted costData for multivariate model")
	exp(predict(model_cost_logs,new_data))

	###############################################################################
	














###############################################################################
# The following questions refer to the data set cigarettecons.xlsx, available in Aula Global.
# If needed, specify the null and alternative hypothesis. Use the 5% level of significance in
# your conclusions.
###############################################################################

	cigarettecons = read.xlsx('cigarettecons.xlsx', sheet=1)

	cigarettecons

	#Exploratory plot
	jpeg("cigaretteconsData.jpeg", width=700, height=750, res=90)
	plot(cigarettecons[,2:ncol(cigarettecons)], pch = 19, 
		col = 'royalblue2', cex = 1.3, gap = .15,
		font.labels = 2, cex.labels = 1.1, 
		main = 'cigarettecons data')
	dev.off()

	#Define critical value
	cigarettecons_significance_level = 0.05 


	###############################################################################
	# (a) Test the hypothesis that the variable Female is not needed in the 
	# regression relating Sales to the six regressor variables.
	###############################################################################

	#Full model
	fc = Sales ~ . - State

	model_cigarettecons = lm(fc, data=cigarettecons)

	#Lets test the hypothesis that Female = 0 
	print("Test hypothesis Female = 0")
	lht(model_cigarettecons, 'Female = 0')

	#With such a high p-value we reject the alterntive hypothesis


	#Another equivalent way to get the same result
	#Reduce model
	fcr = Sales ~ . - State - Female

	model_cigarettecons_reduce1 = lm(fcr, data=cigarettecons)

	#Compare Models
	anova(model_cigarettecons, model_cigarettecons_reduce1)

	#Given the results, it is not needed since
	print("Is the Female variable significant to the model ")
	anova(model_cigarettecons, model_cigarettecons_reduce1)[2,6] < cigarettecons_significance_level

	#P-value for the Female Value
	format(round(anova(model_cigarettecons, model_cigarettecons_reduce1)[2,6], d=4))

	###############################################################################
	# (b) Test the hypothesis that Female and HS are not needed in the above regression equation.
	###############################################################################

	#Test the hypothesis that Female = 0 and HS = 0
	lht(model_cigarettecons, c('Female = 0','HS = 0'))

	#Equivalent way of finding out:
	#Reduce model
	fcrb = Sales ~ . - State - Female - HS

	model_cigarettecons_reduce1_b = lm(fcrb, data=cigarettecons)

	#Compare Models
	anova(model_cigarettecons, model_cigarettecons_reduce1_b)

	#Given the results, it is not needed since
	print("Is the Female and HS variable significant to the model ")
	anova(model_cigarettecons, model_cigarettecons_reduce1_b)[2,6] < cigarettecons_significance_level

	#p-value if the female and hs variables and neeed
	format(round(anova(model_cigarettecons, model_cigarettecons_reduce1_b)[2,6], d=4))

	###############################################################################
	# (c) Obtain the 95% confidence interval for the true regression coefficient of the variable
	# Income.
	###############################################################################

	#The model used for the true regression coefficient will be
	print("Model to be used as true regression coefficient")
	fcrb 

	print("Summary of fitted model")
	summary(model_cigarettecons_reduce1_b)

	#Lets test the significance of the remaining variables
	print("Significance of Age Income Black and Price")
	summary(model_cigarettecons_reduce1_b)$coefficients[-1,4] <= cigarettecons_significance_level

	#test Whether 
	print("Testing linear hypothesis that Black and Age hold no linear relationship with Sales")
	lht(model_cigarettecons_reduce1_b, c('Black = 0','Age = 0'))

	print("p-value of linear hypothesis test of Black and Age against sales")
	format(round(lht(model_cigarettecons_reduce1_b, c('Black = 0','Age = 0'))[2,6], d=4))

	#Create new Model 
	fcrbR = Sales ~ . - State - Female - HS - Age  - Black

	model_cigarettecons_reduce2 = lm(fcrbR, data=cigarettecons)

	print("Summary of new model")
	summary(model_cigarettecons_reduce2)

	print("95% Confidence Interval for variable Income")
	format(round(confint(model_cigarettecons_reduce2, 'Income', level=0.95), d=4))

	#A full model to compared the 95%
	full_cigarettecons_model = Sales ~ . 

	full_cigarretecon =  lm(full_cigarettecons_model, data=cigarettecons)

	print("95% Confidence Interval for variable Income")
	format(round(confint(full_cigarretecon, 'Income', level=0.95), d=4))


	###############################################################################
	# (d) What percentage of the total variation in Sales can be accounted for when Income is
	# removed? Explain.
	###############################################################################
	print("What percentage of the total variation in Sales can be accounted for when Income is removed")
	summary(update(model_cigarettecons_reduce2, ~ . -Income))$r.squared

	#Full model remove Income
	print("What percentage of the total variation in Sales can be accounted for when Income is removed")
	summary(update(full_cigarretecon, ~ . -Income))$adj.r.squared

	#Full model remove Income
	print("What percentage of the total variation in Sales can be accounted for when Income is removed")
	summary(update(full_cigarretecon, ~ . -Income))$r.squared

	###############################################################################
	# (e) What percentage of the total variation in Sales can be accounted for by the three
	# variables Price, Age and Income? Explain.
	###############################################################################
	print("percentage of the total variation in Sales can be accounted for by the three variables Price, Age and Income")
	format(round(summary(update(model_cigarettecons, ~ . - HS - Black - Female))$adj.r.squared*100, d=4))


	###############################################################################
	# (f) What percentage of the total variation in Sales can be accounted for by the variable
	# Income, when Sales is regressed only on this predictor? Explain.
	###############################################################################
	print("What percentage of the total variation in Sales can be accounted for by the variable Income, when Sales is regressed only on this predictor")
	format(round(summary(lm(Sales ~ Income, cigarettecons))$r.squared*100, d=4))

















###############################################################################
# Load the data set state.x77, available in R, and consider fitting a multiple linear regression
# model to explain the response variable Life Expectancy (Y) as a function of the regressors:
# Population; Income; Illiteracy; the Murder rate; the percentage of HS Graduates; Frost,
# the number of days per year with frost; and Area of the state. 
#
# Specific meaning and units of
# measurement of the variables can be obtained with help(state.x77). Using the function
# lm of R, establish that Life expectancy is essentially a linear function of the predictors
# Population, Murder, HS Graduates, and Frost. Population and Area are size type variables.
# Are they better reexpressed in logs?
###############################################################################

#Loading the data
stateData = data.frame(state.x77)

#Define critical value
state_significance_level = 0.05 

#Exploratory plot
jpeg("stateData.jpeg", width=700, height=750, res=90)
plot(stateData, pch = 19, 
	col = 'royalblue2', cex = 1.3, gap = .15,
	font.labels = 2, cex.labels = 1.1, 
	main = 'State data')
dev.off()

#Lets define over which variable to fit a regression model
f_state = Life.Exp ~ . 

#Fit the model
model_state = lm(f_state, data = stateData)

#Show the coefficients
print ("coefficients of the fitted model of state")
format(round(with(model_state, coefficients), d=4))

#Show the summary 
print("Summary of the fitted model of state")
summary(model_state)

#P-values of each regressor
print("P-values of each regressor")
format(round(summary(model_state)$coefficients[-1,4], d=4))

#
print("Test p-values significance")
summary(model_state)$coefficients[-1,4] < state_significance_level

#Test linear hypothesis that Income, illiteracy and area are not needed
print("Linear Hypothesis test that Income, Illiteracy and Area are not needed")
format(round(lht(model_state, c('Income = 0','Illiteracy = 0', 'Area = 0'))[2,6], d=4)) 

# Compare against critical value
print("Are Income, Illiteracy and Area are not needed?")
format(round(lht(model_state, c('Income = 0','Illiteracy = 0', 'Area = 0'))[2,6], d=4))  <=  state_significance_level

#Update the model
f_state_reduced = Life.Exp ~ Population + Murder + HS.Grad + Frost 

#Fit new model
model_state_reduced = lm(f_state_reduced, data = stateData)

#Show the coefficients
print ("coefficients of the fitted model of state")
format(round(with(model_state_reduced, coefficients), d=4))

#Show the summary 
print("Summary of the fitted model of state")
summary(model_state_reduced)


#P-values of each regressor
print("P-values of each regressor")
format(round(summary(model_state_reduced)$coefficients[-1,4], d=4))

#
print("Test p-values significance")
summary(model_state_reduced)$coefficients[-1,4] < state_significance_level








#
#
# Test whether adding logs to population and area improves the model
#
#
stateDataTransformed = transform(stateData, Population = log(Population),
											Income = Income,
											Illiteracy = Illiteracy,
											Life.Exp = Life.Exp,
											Murder = Murder,
											HS.Grad = HS.Grad,
											Frost = Frost,
											Area = log(Area))

#Exploratory plot 
jpeg("stateDataWithLogs.jpeg", width=700, height=750, res=90)
plot(stateDataTransformed, pch = 19, 
	col = 'royalblue2', cex = 1.3, gap = .15,
	font.labels = 2, cex.labels = 1.1, 
	main = 'State data with logs')
dev.off()

#Lets define over which variable to fit a regression model
f_state_transformed = Life.Exp ~ log(Population) + Income + Illiteracy + Murder + HS.Grad + Frost + log(Area)

#Fit the model
model_state_logs = lm(f_state_transformed, data = stateData)


#Show the coefficients
print ("coefficients of the fitted model of state")
format(round(with(model_state_logs, coefficients), d=4))

#Show the summary 
print("Summary of the fitted model of state")
summary(model_state_logs)

#P-values of each regressor
print("P-values of each regressor")
format(round(summary(model_state_logs)$coefficients[-1,4], d=4))

# Compare against critical value
print("Are Income, Illiteracy and Area are not needed?")
format(round(lht(model_state_logs, c('Income = 0','Illiteracy = 0', 'log(Area) = 0'))[2,6], d=4))  <=  state_significance_level

# Compare against critical value
print("Are Income, Illiteracy and Area are not needed?")
format(round(lht(model_state_logs, c('Income = 0','Illiteracy = 0', 'log(Area) = 0'))[2,6], d=4))



#Lets define over which variable to fit a regression model
f_state_transformed_reduced = Life.Exp ~ log(Population) + Murder + HS.Grad + Frost 

#Fit the model
model_state_logs_reduced = lm(f_state_transformed_reduced, data = stateData)


#Show the coefficients
print ("coefficients of the fitted model of state")
format(round(with(model_state_logs_reduced, coefficients), d=4))

#Show the summary 
print("Summary of the fitted model of state")
summary(model_state_logs_reduced)

#P-values of each regressor
print("P-values of each regressor")
format(round(summary(model_state_logs_reduced)$coefficients[-1,4], d=4))







