#######################
# read data , first class
#######################

setwd("/home/alessandro/git/predictive-modeling");

(used.car = read.table('usedcar.txt'));

colnames(used.car) = c('price','years');

class(used.car);

str(used.car);

n = dim(used.car)[1];

##################################
# plot data 
################################## 

with(used.car,plot(years,price, pch = 16,   
main = 'Used car data',
col = 'royalblue', lwd = 1.5));  

####################################
# interpolate several lines visually 
#################################### 

with(used.car,plot(years,price, pch = 16,   
main = 'Used car data',
col = 'royalblue', lwd = 1.5));  

intercept = 14;

slope = -1.;

abline(a = intercept, b = slope, lty = '1373', 
col = 'red',
lwd = 2.5);

intercept = 12;

slope = -1.;

abline(a = intercept, b = slope, lty = '1373', 
col = 'mediumseagreen',
lwd = 2.5);

############################
# Remove significance stars;
############################

options(show.signif.stars = F);   #turns of the significant start showing

#############################################################
# Fit of simple linear regression model  
# For writing '~', do 'AltGr + 4 + Space';
# Price as a function of years 
#############################################################

f = price ~ years; # f: formula, here we define that price is a function of years

class(f);   # the class is a formula

# or simply f = price ~ . ;   this defines price is a function of the data columns
#
#
#

model = lm(f, data = used.car);  #lm is important, function of R

#
# lm -> fitting linear models
#
#

summary(model); 

str(model); 

# This is a list
# This is a list that has 12 objects
# to get the first element out
with(model, coefficients);

#what about attr *** ??? 
attr(model, "names");   # this list something

attr( with(model, coefficients), "names");


#######################
str(summary(model));  

###################################################################################################################
# Standard ANOVA table;
#######################

f = price ~ 1.;

one.sample = lm(f,used.car); 

anova(one.sample,model);

###############
# Model matrix;
###############

model.matrix(model)[,]; # [,] for extraction;

############################################
# Coefficients, fitted values and residuals;
############################################

(betas = coef(model)); # 

betas = coef(model);

#Print again the plot

with(used.car,plot(years,price, pch = 16,   
main = 'Used car data',
col = 'royalblue', lwd = 1.5));  

intercept = betas[1];

slope = betas[2];

abline(a = intercept, b = slope, lty = '1373', 
col = 'red',
lwd = 2.5);


###########

# The fitted values

# fit = with(model, fitted.values);  this is equivalent to the one below
# compared to actual values with(used.car, price)
#                           with(used.car, years)

fit = fitted(model); #values on the line

res = residuals(model);   # diff betweens the truth with the fitted values
#
# What are the residuals for?
#  
# To know the noise, error, of our data. 

# 
# The estimation of Sigma Squared is in the str(model) in sigma!
str(summary(model));

with(summary(model), sigma^2); # this is the squared root
with(summary(model), sigma);

#
# df -> degree of freedom, number of paramaters in min function
#

stres = rstandard(model);  #
# rstandard is the residual divided by its standard deviation.
#
# 

studres = rstudent(model); #Studentized residuals
# is for tstudent distribution over the data
#
#

##########################
# INvestigate significance

LS.fit = with(summary(model), coefficients)
format(round(LS.fit, d =4))

# 
# p  is almost 0 so there is a strong evidence
#   Pr(>|t|) is the probability 
#  this indicates the price of the car decreases with age. 


#windows(); 
X11()
par(mfrow = c(2,2));

plot(model,which = c(1:2),
add.smooth = FALSE, 
label.id = NULL,
sub.caption = '', 
caption = 'residual plots');

library(car);

X11()

residualPlots(model, 
type = 'rstudent',
tests = F, 
quadratic = NULL,
fitted = F); 
 
#################################################
# Confidence intervals for individual parameters;
#################################################

confint(model, 'years', level = 0.90);

#####################################################################
# Confidence and prediction intervals for specified regressor values;
#####################################################################

predict(model, data.frame(years = 5), 
se.fit = TRUE, interval = 'confidence', 
level = 0.95);
 




